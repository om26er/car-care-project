from django.contrib import admin
from django.contrib.auth.models import Group
# from rest_framework.authtoken.models import Token

from care_app import models


class UserAdmin(admin.ModelAdmin):
    class Meta:
        model = models.User


class VehicleTypeAdmin(admin.ModelAdmin):
    class Meta:
        model = models.VehicleType


class VehicleModelInline(admin.StackedInline):
    model = models.VehicleModel


class VehicleMakeAdmin(admin.ModelAdmin):
    inlines = [VehicleModelInline]

    class Meta:
        model = models.VehicleMake


class SubMechanicServiceInline(admin.StackedInline):
    model = models.SubMechanicService


class MechanicServiceAdmin(admin.ModelAdmin):
    inlines = [SubMechanicServiceInline]

    class Meta:
        model = models.MechanicService


class CarWashServiceAdmin(admin.ModelAdmin):
    class Meta:
        model = models.CarWashService


class TowServiceAdmin(admin.ModelAdmin):
    class Meta:
        model = models.TowingService


# class SubCoreServiceInline(admin.StackedInline):
#     model = models.SubCoreService
#
#
# class CoreServiceAdmin(admin.ModelAdmin):
#     inlines = [SubCoreServiceInline]
#
#     class Meta:
#         model = models.CoreService


admin.site.unregister(Group)
# admin.site.unregister(Token)
admin.site.register(models.User, UserAdmin)
admin.site.register(models.VehicleType, VehicleTypeAdmin)
admin.site.register(models.VehicleMake, VehicleMakeAdmin)
admin.site.register(models.MechanicService, MechanicServiceAdmin)
admin.site.register(models.CarWashService, CarWashServiceAdmin)
admin.site.register(models.TowingService, TowServiceAdmin)
# admin.site.register(models.CoreService, CoreServiceAdmin)
