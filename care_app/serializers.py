from collections import OrderedDict

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers, exceptions, validators
from rest_framework.exceptions import ValidationError

from care_app import models


class RichPrimaryKeyRelatedField(serializers.RelatedField):
    # Taken from: https://gist.github.com/AndrewIngram/5c79a3e99ccd20245613
    default_error_messages = serializers.PrimaryKeyRelatedField.default_error_messages

    def __init__(self, serializer, **kwargs):
        self.many = kwargs.get('many', False)
        self.serializer = serializer
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        try:
            return self.get_queryset().get(pk=data)
        except ObjectDoesNotExist:
            self.fail('does_not_exist', pk_value=data)
        except (TypeError, ValueError):
            self.fail('incorrect_type', data_type=type(data).__name__)

    def to_representation(self, value):
        return self.serializer(value, many=self.many).data


def validate_location(location):
    splitted = location.split(',')
    if len(splitted) != 2:
        raise exceptions.ValidationError("Invalid geo location format")
    return location


# class SubCoreServiceSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = models.SubCoreService
#         fields = ('id', 'name', )
#
#
# class CoreServiceSerializer(serializers.ModelSerializer):
#     sub_core_services = SubCoreServiceSerializer(many=True)
#
#     class Meta:
#         model = models.CoreService
#         fields = ('id', 'name', 'sub_core_services')


class VehicleTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VehicleType
        fields = '__all__'


class VehicleModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VehicleModel
        fields = ('id', 'name', )


class VehicleMakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VehicleMake
        fields = ('id', 'name', )


class VehicleMakeWithModelsSerializer(serializers.ModelSerializer):
    models = VehicleModelSerializer(many=True)

    class Meta:
        model = models.VehicleMake
        fields = '__all__'


class CustomerSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    address_coordinates = serializers.CharField(required=True)
    vehicle_type = RichPrimaryKeyRelatedField(VehicleTypeSerializer, queryset=models.VehicleType.objects.all())
    vehicle_make = RichPrimaryKeyRelatedField(VehicleMakeSerializer, queryset=models.VehicleMake.objects.all())
    vehicle_year = serializers.CharField(required=True)
    vehicle_model = RichPrimaryKeyRelatedField(VehicleModelSerializer, queryset=models.VehicleModel.objects.all())

    class Meta:
        model = models.User
        fields = ('id', 'email', 'username', 'password', 'user_type', 'name', 'contact_number', 'address',
                  'profile_photo', 'address_coordinates', 'vehicle_type', 'vehicle_make', 'vehicle_year',
                  'vehicle_model')

    def validate_address_coordinates(self, coord):
        return validate_location(coord)


class ProviderSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    contact_person = serializers.CharField(required=True)
    provider_type = serializers.ChoiceField(choices=models.PROVIDER_CHOICES, required=True)
    provider_description = serializers.CharField(required=True)

    class Meta:
        model = models.User
        fields = ('id', 'email', 'username', 'password', 'user_type', 'name', 'contact_number', 'address',
                  'profile_photo', 'contact_person', 'address_coordinates', 'provider_type', 'provider_description',
                  'freelancer_certificate')

    def validate_address_coordinates(self, coord):
        return validate_location(coord)

    def validate(self, attrs):
        if 'request' in self.context and self.context['request'].method == 'POST':
            if attrs['provider_type'] == 'freelancer' and 'freelancer_certificate' not in attrs:
                raise ValidationError({'freelancer_certificate': ['Require for freelancer']}, 400)
        return super().validate(attrs)


class CarPartSerializer(serializers.ModelSerializer):
    make = RichPrimaryKeyRelatedField(VehicleMakeSerializer, queryset=models.VehicleMake.objects.all())
    model = RichPrimaryKeyRelatedField(VehicleModelSerializer, queryset=models.VehicleModel.objects.all())
    contact = serializers.SerializerMethodField()

    class Meta:
        model = models.CarPart
        fields = '__all__'

    def get_contact(self, obj):
        return obj.supplier.contact_number


class CarPartSearchParametersSerializer(serializers.Serializer):
    make = serializers.PrimaryKeyRelatedField(queryset=models.VehicleMake.objects.all())
    model = serializers.PrimaryKeyRelatedField(queryset=models.VehicleModel.objects.all())
    year = serializers.IntegerField(required=True)


class SubMechanicServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SubMechanicService
        fields = ('id', 'name')


class MechanicServiceSerializer(serializers.ModelSerializer):
    sub_services = SubMechanicServiceSerializer(many=True)

    class Meta:
        model = models.MechanicService
        fields = ('id', 'name', 'sub_services')


class MechanicServiceWithPriceSerializer(serializers.ModelSerializer):
    contact = serializers.SerializerMethodField()
    service = RichPrimaryKeyRelatedField(SubMechanicServiceSerializer, queryset=models.SubMechanicService.objects.all())
    provider = serializers.PrimaryKeyRelatedField(
        write_only=True, queryset=models.User.objects.filter(user_type=models.TYPE_ACCOUNT_PROVIDER))

    class Meta:
        model = models.MechanicServiceWithPrice
        fields = '__all__'
        validators = [
            validators.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('provider', 'service')
            )
        ]

    def get_contact(self, obj):
        return obj.provider.contact_number


class MechanicSearchRequestValidator(serializers.Serializer):
    service = serializers.PrimaryKeyRelatedField(
        queryset=models.SubMechanicService.objects.all(), required=True, many=True)
    base_location = serializers.CharField(required=True)

    def validate_base_location(self, coord):
        return validate_location(coord)


class CarWashServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CarWashService
        fields = '__all__'


class CarWashServiceWithPriceSerializer(serializers.ModelSerializer):
    contact = serializers.SerializerMethodField()
    service = RichPrimaryKeyRelatedField(CarWashServiceSerializer, queryset=models.CarWashService.objects.all())

    class Meta:
        model = models.CarWashServiceWithPrice
        fields = '__all__'
        validators = [
            validators.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('provider', 'service')
            )
        ]

    def get_contact(self, obj):
        return obj.provider.contact_number


class CarWashSearchRequestValidator(serializers.Serializer):
    service = serializers.PrimaryKeyRelatedField(queryset=models.CarWashService.objects.all(), required=True)
    base_location = serializers.CharField(required=True)

    def validate_base_location(self, coord):
        return validate_location(coord)


class TowingServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TowingService
        fields = '__all__'


class TowingServiceWithPriceSerializer(serializers.ModelSerializer):
    contact = serializers.SerializerMethodField()
    service = RichPrimaryKeyRelatedField(TowingServiceSerializer, queryset=models.TowingService.objects.all())

    class Meta:
        model = models.TowingServiceWithPrice
        fields = '__all__'
        validators = [
            validators.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('provider', 'service')
            )
        ]

    def get_contact(self, obj):
        return obj.provider.contact_number


class TowingSearchRequestValidator(serializers.Serializer):
    service = serializers.PrimaryKeyRelatedField(queryset=models.TowingService.objects.all(), required=True)
    base_location = serializers.CharField(required=True)

    def validate_base_location(self, coord):
        return validate_location(coord)


class MechanicProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MechanicProfile
        fields = '__all__'


class ProviderCompactSerializer(serializers.ModelSerializer):
    service_price = serializers.SerializerMethodField()
    profile = serializers.SerializerMethodField()

    class Meta:
        model = models.User
        fields = ('id', 'name', 'contact_number', 'address', 'profile_photo', 'contact_person', 'address_coordinates',
                  'service_price', 'profile')

    def get_service_price(self, obj):
        request = self.context['request']
        if self.context['request_type'] == 'car_wash_search':
            validator = CarWashSearchRequestValidator(data=request.query_params)
            validator.is_valid(True)
            data = dict(validator.validated_data)
            service = models.CarWashServiceWithPrice.objects.get(provider=obj, service=data['service'])
        elif self.context['request_type'] == 'mechanic_search':
            validator = MechanicSearchRequestValidator(data=request.query_params)
            validator.is_valid(True)
            data = dict(validator.validated_data)
            service = models.MechanicServiceWithPrice.objects.filter(provider=obj, service=data['service']).first()
        elif self.context['request_type'] == 'towing_search':
            validator = TowingSearchRequestValidator(data=request.query_params)
            validator.is_valid(True)
            data = dict(validator.validated_data)
            service = models.TowingServiceWithPrice.objects.get(provider=obj, service=data['service'])
        else:
            return ''

        return str(service.price)

    def get_profile(self, obj):
        if self.context['request_type'] == 'mechanic_search':
            try:
                profile = models.MechanicProfile.objects.get(user=obj)
                serializer = MechanicProfileSerializer(instance=profile)
                return serializer.data
            except models.MechanicProfile.DoesNotExist:
                pass
        return ''
