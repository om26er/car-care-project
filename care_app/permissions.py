from care_app import models

from rest_framework import permissions


class IsProvider(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.user_type == models.TYPE_ACCOUNT_PROVIDER


class IsProviderOrReadOnly(IsProvider):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS or super().has_permission(request, view)


class IsOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return obj.provider == request.user
