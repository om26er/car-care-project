from django.apps import AppConfig


class CareAppConfig(AppConfig):
    name = 'care_app'
