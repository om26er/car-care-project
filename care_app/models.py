from django.db import models

from simple_login import models as dsl_models

TYPE_ACCOUNT_ADMIN = 1
TYPE_ACCOUNT_CUSTOMER = 2
TYPE_ACCOUNT_PROVIDER = 3

ACCOUNT_CHOICES = (
    (TYPE_ACCOUNT_ADMIN, 'Admin'),
    (TYPE_ACCOUNT_CUSTOMER, 'Customer'),
    (TYPE_ACCOUNT_PROVIDER, 'Provider'),
)

PROVIDER_CHOICES = (
    ('company', 'company'),
    ('contractor', 'contractor'),
    ('freelancer', 'freelancer'),
    ('seller', 'seller'),
)


class VehicleType(models.Model):
    name = models.CharField(max_length=128, blank=False)

    def __str__(self):
        return self.name


class VehicleMake(models.Model):
    name = models.CharField(max_length=128, blank=False)

    def __str__(self):
        return self.name


class VehicleModel(models.Model):
    make = models.ForeignKey(VehicleMake, on_delete=models.CASCADE, related_name='models')
    name = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return self.name


class User(dsl_models.BaseUser):
    account_activation_sms_otp = None
    password_reset_sms_otp = None

    # Core fields
    user_type = models.IntegerField(blank=False, choices=ACCOUNT_CHOICES, default=TYPE_ACCOUNT_ADMIN)
    username = models.CharField(max_length=255, blank=False, unique=True)
    email = models.EmailField(max_length=255, blank=False, unique=True)

    # Common fields
    name = models.CharField(max_length=255, blank=False)
    address = models.CharField(max_length=512, blank=False)
    contact_number = models.CharField(max_length=128, blank=False)
    address_coordinates = models.CharField(max_length=128, blank=False)
    profile_photo = models.ImageField(blank=True)

    # Customer specific fields
    vehicle_type = models.ForeignKey(VehicleType, on_delete=models.CASCADE, blank=True, null=True)
    vehicle_make = models.ForeignKey(VehicleMake, on_delete=models.CASCADE, blank=True, null=True)
    vehicle_model = models.ForeignKey(VehicleModel, on_delete=models.CASCADE, blank=True, null=True)
    vehicle_year = models.CharField(max_length=32, blank=False)

    # Provider specific fields
    contact_person = models.CharField(max_length=128, blank=True)
    provider_type = models.CharField(max_length=255, blank=True, choices=PROVIDER_CHOICES)
    provider_description = models.CharField(max_length=512, blank=True)
    freelancer_certificate = models.FileField(blank=True)


class CarPart(models.Model):
    supplier = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    description = models.CharField(max_length=255, blank=False)
    model = models.ForeignKey(VehicleModel, on_delete=models.CASCADE)
    make = models.ForeignKey(VehicleMake, on_delete=models.CASCADE)
    start_year = models.IntegerField(blank=False)
    end_year = models.IntegerField(blank=False)
    price = models.CharField(max_length=64, blank=False)
    image = models.ImageField(blank=False)


class MechanicService(models.Model):
    name = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return self.name


class SubMechanicService(models.Model):
    service = models.ForeignKey(MechanicService, on_delete=models.CASCADE, related_name='sub_services')
    name = models.CharField(max_length=255, blank=False)


class MechanicServiceWithPrice(models.Model):
    provider = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    service = models.ForeignKey(SubMechanicService, on_delete=models.CASCADE, blank=False)
    price = models.CharField(max_length=255, blank=False)


class MechanicProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=False)
    start_time = models.TimeField()
    end_time = models.TimeField()
    experience = models.CharField(max_length=64, blank=False)


class CarWashService(models.Model):
    name = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return self.name


class CarWashServiceWithPrice(models.Model):
    provider = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    service = models.ForeignKey(CarWashService, on_delete=models.CASCADE, blank=False)
    price = models.CharField(max_length=255, blank=False)


class TowingService(models.Model):
    name = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return self.name


class TowingServiceWithPrice(models.Model):
    provider = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    service = models.ForeignKey(TowingService, on_delete=models.CASCADE, blank=False)
    price = models.CharField(max_length=255, blank=False)


# class CoreService(models.Model):
#     name = models.CharField(max_length=255, blank=False)
#
#     def __str__(self):
#         return self.name
#
#
# class SubCoreService(models.Model):
#     core_service = models.ForeignKey(CoreService, on_delete=models.CASCADE, related_name='sub_core_services')
#     name = models.CharField(max_length=255, blank=False)
