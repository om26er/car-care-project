from geopy.distance import vincenty


def get_closest_mechanics_first(base_location, mechanics):
    def sort_closest(item):
        return vincenty(
            tuple(base_location.split(',')),
            tuple(item.address_coordinates.split(','))
        ).kilometers
    return sorted(mechanics, key=sort_closest)


def filter_mechanics_by_radius(base_location, mechanics, radius):
    return [mechanic for mechanic in mechanics
            if vincenty(tuple(base_location.split(',')),
                        tuple(mechanic.address_coordinates.split(','))).kilometers <= int(radius)]
