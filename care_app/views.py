from django.shortcuts import get_object_or_404
from django.db.models import Q
from rest_framework.exceptions import ValidationError
from rest_framework import status, generics, views as r_views, response, permissions as r_permissions

from simple_login import views

from care_app import models, serializers, permissions, helpers


class RegisterCustomerAPIView(views.RegisterAPIView):
    serializer_class = serializers.CustomerSerializer

    def post(self, request, *args, **kwargs):
        request.data.update({'user_type': models.TYPE_ACCOUNT_CUSTOMER})
        return super().post(request, *args, **kwargs)


class RegisterProviderAPIView(views.RegisterAPIView):
    serializer_class = serializers.ProviderSerializer

    def post(self, request, *args, **kwargs):
        request.data.update({'user_type': models.TYPE_ACCOUNT_PROVIDER})
        return super().post(request, *args, **kwargs)


def get_serializer_class(request):
    if not request.user.is_authenticated:
        if 'username' in request.data:
            try:
                user = models.User.objects.get(username=request.data.get('username'))
            except models.User.DoesNotExist:
                raise ValidationError("User does not exist", status.HTTP_404_NOT_FOUND)
        elif 'email' in request.data:
            try:
                user = models.User.objects.get(email=request.data.get('email'))
            except models.User.DoesNotExist:
                raise ValidationError("User does not exist", status.HTTP_404_NOT_FOUND)
        else:
            raise ValidationError('Must provide either username or email to activate', status.HTTP_400_BAD_REQUEST)
    else:
        user = request.user

    if user.user_type == models.TYPE_ACCOUNT_CUSTOMER:
        return serializers.CustomerSerializer
    elif user.user_type == models.TYPE_ACCOUNT_PROVIDER:
        return serializers.ProviderSerializer
    else:
        raise ValidationError("Unsupported user type", status.HTTP_400_BAD_REQUEST)


class ActivateAPIView(views.ActivationAPIView):
    user_model = models.User

    def get_serializer_class(self):
        return get_serializer_class(self.request)


class ActivationKeyRequestAPIView(views.ActivationKeyRequestAPIView):
    user_model = models.User


class LoginAPIView(views.LoginAPIView):
    user_model = models.User

    def get_serializer_class(self):
        return get_serializer_class(self.request)


class ProfileAPIView(views.RetrieveUpdateDestroyProfileAPIView):
    user_model = models.User

    def get_serializer_class(self):
        return get_serializer_class(self.request)

    def get(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        serializer = serializer_class(self.get_auth_user(), context={'request': self.request})
        return response.Response(serializer.data, status=status.HTTP_200_OK)


class ForgotPasswordAPIView(views.PasswordResetRequestAPIView):
    user_model = models.User


class ChangePasswordAPIView(views.PasswordChangeAPIView):
    user_model = models.User


class StatusAPIView(views.StatusAPIView):
    user_model = models.User


class VehicleMakeListAPIView(generics.ListAPIView):
    serializer_class = serializers.VehicleMakeSerializer
    queryset = models.VehicleMake.objects.all()


class VehicleMakeWithModelsListAPIView(generics.ListAPIView):
    serializer_class = serializers.VehicleMakeWithModelsSerializer
    queryset = models.VehicleMake.objects.all()


class ModelsByMakeAPIView(generics.ListAPIView):
    serializer_class = serializers.VehicleModelSerializer

    def get_queryset(self):
        return models.VehicleModel.objects.filter(make__id=self.kwargs['id'])


class VehicleTypeListAPIView(generics.ListAPIView):
    serializer_class = serializers.VehicleTypeSerializer
    queryset = models.VehicleType.objects.all()


class ListCreateSupplierCarPartAPIView(generics.ListCreateAPIView):
    serializer_class = serializers.CarPartSerializer
    permission_classes = (permissions.IsProvider, )

    def get_queryset(self):
        return models.CarPart.objects.filter(supplier=self.request.user)

    def post(self, request, *args, **kwargs):
        self.request.data.update({'supplier': self.request.user.id})
        return super().post(request, *args, **kwargs)


class RetrieveUpdateDestroySupplierCarPartAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.CarPartSerializer
    permission_classes = (permissions.IsProvider,)

    def get_queryset(self):
        return models.CarPart.objects.filter(supplier=self.request.user)


class PartSearchAPIView(generics.ListAPIView):
    serializer_class = serializers.CarPartSerializer

    def get_queryset(self):
        validator = serializers.CarPartSearchParametersSerializer(data=self.request.query_params)
        validator.is_valid(True)
        data = dict(validator.validated_data)
        year = data.pop('year')
        data.update({'start_year': year, 'end_year': year})
        return models.CarPart.objects.filter(**data)


class MechanicServicesListAPIView(generics.ListAPIView):
    serializer_class = serializers.MechanicServiceSerializer
    queryset = models.MechanicService.objects.all()
    pagination_class = None


class MechanicServiceWithPriceSerializerListCreateAPIView(r_views.APIView):
    serializer_class = serializers.MechanicServiceWithPriceSerializer
    permission_classes = (permissions.IsProvider, )

    # def get_queryset(self):
    #     return models.MechanicServiceWithPrice.objects.filter(provider=self.request.user)

    def get(self, *args, **kwargs):
        services = models.MechanicServiceWithPrice.objects.filter(provider=self.request.user)
        print(services)
        serializer = self.serializer_class(instance=services, many=True)
        return response.Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        if isinstance(self.request.data, list):
            for obj in self.request.data:
                obj.update({'provider': self.request.user.id})
        serializer = self.serializer_class(data=self.request.data, many=True)
        serializer.is_valid(True)
        serializer.save()
        return response.Response(data=serializer.data, status=status.HTTP_201_CREATED)


class MechanicServiceRetriesUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.MechanicServiceWithPriceSerializer
    permission_classes = (permissions.IsProvider, )

    def get_queryset(self):
        return models.MechanicServiceWithPrice.objects.filter(provider=self.request.user)


class MechanicServiceFinder(generics.ListAPIView):
    serializer_class = serializers.ProviderCompactSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_type'] = 'mechanic_search'
        return context

    def get_queryset(self):
        validator = serializers.MechanicSearchRequestValidator(data=self.request.query_params)
        validator.is_valid(raise_exception=True)
        data = dict(validator.validated_data)
        queryset = models.User.objects.filter(user_type=models.TYPE_ACCOUNT_PROVIDER)
        for s in data['service']:
            queryset = queryset.filter(mechanicservicewithprice__service=s)
        return helpers.get_closest_mechanics_first(data['base_location'], queryset)


class MechanicProfileAPIView(r_views.APIView):
    permission_classes = (permissions.IsProvider, )

    def get(self, *args, **kwargs):
        obj = get_object_or_404(models.MechanicProfile, user=self.request.user)
        serializer = serializers.MechanicProfileSerializer(instance=obj)
        return response.Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, *args, **kwargs):
        self.request.data.update({'user': self.request.user.id})
        serializer = serializers.MechanicProfileSerializer(data=self.request.data)
        serializer.is_valid(True)
        serializer.save()
        return response.Response(data=serializer.data, status=status.HTTP_201_CREATED)

    def _update(self, partial=True):
        obj = get_object_or_404(models.MechanicProfile, user=self.request.user)
        if 'user' in self.request.data:
            self.request.data.pop('user')
        if not partial:
            self.request.data.update({'user': self.request.user.id})
        serializer = serializers.MechanicProfileSerializer(instance=obj, data=self.request.data, partial=partial)
        serializer.is_valid(True)
        serializer.save()
        return response.Response(data=serializer.data, status=status.HTTP_200_OK)

    def patch(self, *args, **kwargs):
        return self._update(partial=True)

    def put(self, *args, **kwargs):
        return self._update(partial=False)


class CarWashServiceListAPIView(generics.ListAPIView):
    serializer_class = serializers.CarWashServiceSerializer
    queryset = models.CarWashService.objects.all()


class CarWashServiceWithPriceSerializerListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = serializers.CarWashServiceWithPriceSerializer
    permission_classes = (permissions.IsProvider, )

    def get_queryset(self):
        return models.CarWashServiceWithPrice.objects.filter(provider=self.request.user)

    def post(self, request, *args, **kwargs):
        self.request.data.update({'provider': self.request.user.id})
        return super().post(request, *args, **kwargs)


class CarWashServiceWithPriceRetriesUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.CarWashServiceWithPriceSerializer
    permission_classes = (permissions.IsProvider, )

    def get_queryset(self):
        return models.CarWashServiceWithPrice.objects.filter(provider=self.request.user)


class CarWashServiceFinder(generics.ListAPIView):
    serializer_class = serializers.ProviderCompactSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_type'] = 'car_wash_search'
        return context

    def get_queryset(self):
        validator = serializers.CarWashSearchRequestValidator(data=self.request.query_params)
        validator.is_valid(raise_exception=True)
        data = dict(validator.validated_data)
        queryset = models.User.objects.filter(
            user_type=models.TYPE_ACCOUNT_PROVIDER, carwashservicewithprice__service=data['service'])
        return helpers.get_closest_mechanics_first(data['base_location'], queryset)


class TowingServiceListAPIView(generics.ListAPIView):
    serializer_class = serializers.TowingServiceSerializer
    queryset = models.TowingService.objects.all()


class TowingServiceWithPriceSerializerListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = serializers.TowingServiceWithPriceSerializer
    permission_classes = (permissions.IsProvider, )

    def get_queryset(self):
        return models.TowingServiceWithPrice.objects.filter(provider=self.request.user)

    def post(self, request, *args, **kwargs):
        self.request.data.update({'provider': self.request.user.id})
        return super().post(request, *args, **kwargs)


class TowingServiceWithPriceRetriesUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.TowingServiceWithPriceSerializer
    permission_classes = (permissions.IsProvider, )

    def get_queryset(self):
        return models.TowingServiceWithPrice.objects.filter(provider=self.request.user)


class TowingServiceFinder(generics.ListAPIView):
    serializer_class = serializers.ProviderCompactSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_type'] = 'towing_search'
        return context

    def get_queryset(self):
        validator = serializers.TowingSearchRequestValidator(data=self.request.query_params)
        validator.is_valid(raise_exception=True)
        data = dict(validator.validated_data)
        queryset = models.User.objects.filter(
            user_type=models.TYPE_ACCOUNT_PROVIDER, towingservicewithprice__service=data['service'])
        return helpers.get_closest_mechanics_first(data['base_location'], queryset)


# class CoreServiceListAPIView(generics.ListAPIView):
#     queryset = models.CoreService.objects.all()
#     serializer_class = serializers.CoreServiceSerializer
#     pagination_class = None
