from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from care_app import views


urlpatterns = [
    path('api/register-customer', views.RegisterCustomerAPIView.as_view()),
    path('api/register-provider', views.RegisterProviderAPIView.as_view()),
    path('api/request-activation-key', views.ActivationKeyRequestAPIView.as_view()),
    path('api/activate', views.ActivateAPIView.as_view()),
    path('api/login', views.LoginAPIView.as_view()),
    path('api/forgot-password', views.ForgotPasswordAPIView.as_view()),
    path('api/change-password', views.ChangePasswordAPIView.as_view()),
    path('api/status', views.StatusAPIView.as_view()),
    path('api/me', views.ProfileAPIView.as_view()),

    path('api/vehicles/make', views.VehicleMakeListAPIView.as_view()),
    path('api/vehicles/make-with-models', views.VehicleMakeWithModelsListAPIView.as_view()),
    path('api/vehicles/make/<int:id>/models', views.ModelsByMakeAPIView.as_view()),
    path('api/vehicles/type', views.VehicleTypeListAPIView.as_view()),

    path('api/provider/parts', views.ListCreateSupplierCarPartAPIView.as_view()),
    path('api/provider/parts/<int:pk>', views.RetrieveUpdateDestroySupplierCarPartAPIView.as_view()),
    path('api/parts', views.PartSearchAPIView.as_view()),

    path('api/mechanic/services', views.MechanicServiceWithPriceSerializerListCreateAPIView.as_view()),
    path('api/mechanic/services/<int:pk>', views.MechanicServiceRetriesUpdateDeleteAPIView.as_view()),
    path('api/mechanic-services', views.MechanicServicesListAPIView.as_view()),
    path('api/mechanic/profile', views.MechanicProfileAPIView.as_view()),
    path('api/find-mechanic', views.MechanicServiceFinder.as_view()),

    path('api/service-station/services', views.CarWashServiceWithPriceSerializerListCreateAPIView.as_view()),
    path('api/service-station/services/<int:pk>', views.CarWashServiceWithPriceRetriesUpdateDeleteAPIView.as_view()),
    path('api/car-wash-services', views.CarWashServiceListAPIView.as_view()),
    path('api/find-service-station', views.CarWashServiceFinder.as_view()),

    path('api/towing-provider/services', views.TowingServiceWithPriceSerializerListCreateAPIView.as_view()),
    path('api/towing-provider/services/<int:pk>', views.TowingServiceWithPriceRetriesUpdateDeleteAPIView.as_view()),
    path('api/towing-services', views.TowingServiceListAPIView.as_view()),
    path('api/find-towing-provider', views.TowingServiceFinder.as_view()),

    # path('api/core-services', views.CoreServiceListAPIView.as_view()),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
